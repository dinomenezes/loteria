$('.tipoJogo').on('change', function geraBlocos(){
    var blocos = $('.tipoJogo').val();
    var inputs = "";
    for(var i = 0; i < blocos; i++){
        inputs +=`
        <div class="form-group col-sm-4">
            <input type="text" class="form-control number`+i+`" value="">
        </div>
        `;
    }

    $('.addJogo').html(inputs);
});


function tableJogos(){
    var casas = $('.tipoJogo').val();
    var jogoAtual = [];
    $('table').html('');
    
    $(".numbers").each(function(index){
        var n = $(this).val();
        if(n > 9){
            jogoAtual.push(parseInt(n.toString().split('')[0]));
        }else{
            jogoAtual.push(0);
        }
    });
    gerador(jogoAtual, casas);
}

function gerador(jogoAtual, qtd){
    var FirstLine = jogoAtual;
    var newLine = [];
    var eachLine = [];
    newLine.push(FirstLine);

    for(var i = 0; i < 10; i++){
    FirstLine.forEach(element => {
        var number = element + 7;

        if(number > 9){
        eachLine.push(parseInt(number.toString().split('')[1]));
        }else{
        eachLine.push(number);
        }
    });
    newLine.push(eachLine);
    var FirstLine = eachLine;
    var eachLine = [];
    }

    newLine.forEach(arr => {
        var line = "<tr>";
        for(var i = 0; i < qtd; i++){
            var style = "";
            if(arr[i] == 0){
                style = "style='background-color:yellow; color:red'";
            }
            line += "<td "+style+">"+arr[i]+"</td>";
        }

        line += "</tr>";
        $('table').append(line);
        line = "";
    })

    $('td').on('click', function(){
        var that = $(this);
        if(that.hasClass('mark')){
            that.removeClass('mark');
        }else{
            that.addClass('mark');
        }
    });
}

